package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener   {
    private DatabaseReference usersRef;
    private FirebaseAuth mAuth;
    private String currentuserid;
    private CardView cardview;
    private ImageView image;
    private Spinner spinner;
    private EditText enter;

    private Button ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Spinner spinner=findViewById(R.id.spinner);
        enter = (EditText) findViewById(R.id.enter);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.Spinner_Items,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

       // detail=(Button) findViewById(R.id.detail);
        ok=(Button) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,ThirdActivity.class);
                startActivity(i);
               // Toast.makeText(MainActivity.this, "select the transaction number", Toast.LENGTH_LONG).show();
            }
        });
        cardview=(CardView) findViewById(R.id.cardview);
        image=(ImageView) findViewById(R.id.image);
        final TextView a=(TextView) findViewById(R.id.username);
        final TextView b=(TextView) findViewById(R.id.balance);
        final TextView c=(TextView) findViewById(R.id.mobile);
        mAuth=FirebaseAuth.getInstance();
        currentuserid=mAuth.getCurrentUser().getUid();
        usersRef= FirebaseDatabase.getInstance().getReference().child("users").child(currentuserid);
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String user=dataSnapshot.child("username").getValue().toString();
                    String balancee=dataSnapshot.child("Balance").getValue().toString();
                    String mobile=dataSnapshot.child("phone").getValue().toString();
                    a.setText("Username : " + user);
                    b.setText("Balance : " + balancee);
                    c.setText("Mobile : " + mobile);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
       /* usersRef= FirebaseDatabase.getInstance().getReference().child("users").child("D62lyNbU1UWk3sAw2mm5LxOgTZN2");
        Query updateQuery=usersRef.child("users").orderByKey().equalTo("users");
        updateQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().getParent().child("Balance").setValue("726");
                // String Total=dataSnapshot.child("balance").getValue().toString();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/








    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.example_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.profile:
                Intent i1=new Intent(this, Profiled.class);
                this.startActivity(i1);
                return true;
            case R.id.logout:
                Intent i2=new Intent(this, LogOutActivity.class);
                this.startActivity(i2);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getItemAtPosition(position).equals("Choose txn")){

        }
        else {
            Intent d=new Intent(MainActivity.this,SecondActivity.class);
            startActivity(d);
           /* usersRef= FirebaseDatabase.getInstance().getReference().child("company").child("Qvo5NQsir4OpaHRdzgaqcWLRfyC2");
            usersRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){

                        String Name=dataSnapshot.child("name").getValue().toString();
                        String Txn=dataSnapshot.child("txn").getValue().toString();

                        detail.setText("Receiver : " + Name+"\n \n"+"Txn : " + Txn);

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {


                }
            });*/

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
