package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

public class CompanyActivity extends AppCompatActivity {
    private EditText username,address,mobile;
    private EditText txn,password;
    private Button registerforcompany;
    private FirebaseAuth mAuth;
    private DatabaseReference usersRef;
    String currentuserid;
    int l;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        mAuth=FirebaseAuth.getInstance();
        currentuserid=mAuth.getCurrentUser().getUid();

        usersRef= FirebaseDatabase.getInstance().getReference().child("company").child(currentuserid);

        username=(EditText) findViewById(R.id.username);
        address=(EditText) findViewById(R.id.address);
        mobile=(EditText) findViewById(R.id.mobile);
        txn=(EditText) findViewById(R.id.txn);
       // txn=(EditText) findViewById(R.id.txn);
        //password=(EditText) findViewById(R.id.password);
        registerforcompany=(Button) findViewById(R.id.registerforcompany);
        registerforcompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveRegisterInformation();
            }

            private void SaveRegisterInformation() {
                String user=username.getText().toString();

                String phone=mobile.getText().toString();
                String pan=txn.getText().toString();
                String addres=address.getText().toString();
                //String pass=password.getText().toString();

                if (TextUtils.isEmpty(user))
                {
                    Toast.makeText(CompanyActivity.this," Enter Username",Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(phone))
                {
                    Toast.makeText(CompanyActivity.this,"Enter Mobile",Toast.LENGTH_SHORT).show();
                    return;

                }else{
                    l = phone.length();
                    if (l != 10 ) {
                        Toast.makeText(CompanyActivity.this, "Mobile number should be of 10 digit", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (TextUtils.isEmpty(pan))
                {
                    Toast.makeText(CompanyActivity.this,"Enter Txn Number",Toast.LENGTH_SHORT).show();
                    return;

                }else {
                    l = pan.length();
                    if (l != 9 ) {
                        Toast.makeText(CompanyActivity.this, "Txn number should be of 9 digit", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (TextUtils.isEmpty(addres))
                {
                    Toast.makeText(CompanyActivity.this,"Enter Address ",Toast.LENGTH_SHORT).show();
                    return;
                }
                /*if (TextUtils.isEmpty(pan))
                {
                    Toast.makeText(CompanyActivity.this,"please enter the txn number",Toast.LENGTH_SHORT).show();
                    return;
                }*/
                /*if (TextUtils.isEmpty(pass))
                {
                    Toast.makeText(CompanyActivity.this,"please enter the password",Toast.LENGTH_SHORT).show();
                    return;
                }*/
                else {
                    HashMap usermap= new HashMap();
                    usermap.put("name",user);
                    usermap.put("address",addres);
                    usermap.put("phone",phone);
                    usermap.put("txn",pan);

                    usermap.put("time", ServerValue.TIMESTAMP);
                    usermap.put("Balance","0");
                    usersRef.updateChildren(usermap).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                SendUserToMainActivity();
                                Toast.makeText(CompanyActivity.this,"Data is registered successfully",Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String message=task.getException().getMessage();
                                Toast.makeText(CompanyActivity.this,"Error :"+message,Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                }

            }
        });
    }
    private void SendUserToMainActivity(){
        Intent main=new Intent(CompanyActivity.this,LoginActivity.class);
        startActivity(main);
    }
}
