package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {
   private EditText username,address,mobile,bankname;
    private Button save;
    private FirebaseAuth mAuth;
    private DatabaseReference usersRef;
    String currentuserid;
    int l;
    int text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth=FirebaseAuth.getInstance();
        currentuserid=mAuth.getCurrentUser().getUid();

        usersRef= FirebaseDatabase.getInstance().getReference().child("users").child(currentuserid);

         username=(EditText) findViewById(R.id.username);
         address=(EditText) findViewById(R.id.address);
         mobile=(EditText) findViewById(R.id.mobile);
         bankname=(EditText) findViewById(R.id.bankname);

         save=(Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveAccountSetupInformation();
            }
        });

    }
    private void SaveAccountSetupInformation(){
        String user=username.getText().toString();
        String addres=address.getText().toString();
        String phone=mobile.getText().toString();
        String bank=bankname.getText().toString();
        if (TextUtils.isEmpty(user))
        {
            Toast.makeText(this,"Enter Username",Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(addres))
        {
            Toast.makeText(this," Enter Address ",Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(phone))
        {
<<<<<<< HEAD
            Toast.makeText(this,"Enter Mobile",Toast.LENGTH_SHORT).show();
            return;

        }else{
            l = phone.length();
            if (l != 10 ) {
                Toast.makeText(ProfileActivity.this, "Mobile number should be of 10 digit", Toast.LENGTH_LONG).show();
                return;
            }

=======
            Toast.makeText(this,"please enter mobile",Toast.LENGTH_SHORT).show();
            return;
        }else{    
            l = phone.length();
            if (l != 10 ) {
               Toast.makeText(ProfileActivity.this, "Enter valid number", Toast.LENGTH_LONG).show();
                return;
            } 
           
>>>>>>> 1252f4316be6907eb8f57c3ed5e764cd97e4af79


        }
        if (TextUtils.isEmpty(bank))
        {
            Toast.makeText(this,"Enter Fullname",Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            HashMap usermap= new HashMap();
            usermap.put("username",user);
            usermap.put("address",addres);
            usermap.put("phone",phone);
            usermap.put("fullname",bank);
            usermap.put("Balance","500");
            usermap.put("time", ServerValue.TIMESTAMP);
            usersRef.updateChildren(usermap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){
                        SendUserToMainActivity();
                        Toast.makeText(ProfileActivity.this,"Your data is saved successfully",Toast.LENGTH_SHORT).show();

                    }
                    else {
                        String message=task.getException().getMessage();
                        Toast.makeText(ProfileActivity.this,"Error :"+message,Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }
    }



    private void SendUserToMainActivity(){
        Intent main=new Intent(ProfileActivity.this,MainActivity.class);
        startActivity(main);
    }
}
