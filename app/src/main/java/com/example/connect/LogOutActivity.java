package com.example.connect;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class LogOutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_out);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(LogOutActivity.this);
        alertDialog.setTitle("Logout");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent l=new Intent(LogOutActivity.this, LoginActivity.class);
                startActivity(l);


            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent d=new Intent(LogOutActivity.this, MainActivity.class);
                startActivity(d);

            }
        });
        alertDialog.show();
    }
}
