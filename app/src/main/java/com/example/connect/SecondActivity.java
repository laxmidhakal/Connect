package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class SecondActivity extends AppCompatActivity {
    private TextView a,b,c,e,f,g;
    private Button d;
    private EditText enter;
    private DatabaseReference usersRef;
    private DatabaseReference usersRef2;
    private FirebaseAuth mAuth;
    private String currentuserid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        a=(TextView) findViewById(R.id.a);
        b=(TextView) findViewById(R.id.b);
        c=(TextView) findViewById(R.id.c);
        e=(TextView) findViewById(R.id.e);
        f=(TextView) findViewById(R.id.f);
        g=(TextView) findViewById(R.id.g);
        enter = (EditText) findViewById(R.id.enter);

        d=(Button) findViewById(R.id.d);
        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent i=new Intent(SecondActivity.this,FourthActivity.class);
                //startActivity(i);
                Add();

            }

            private void Add() {
                final String name = enter.getText().toString().trim();

                if (!TextUtils.isEmpty(name)) {
                   /* mAuth=FirebaseAuth.getInstance();
                    currentuserid=mAuth.getCurrentUser().getUid();
                    usersRef= FirebaseDatabase.getInstance().getReference("users").child(currentuserid);
                    Query updateQuery=usersRef.child("users").orderByKey().equalTo("users");
                    updateQuery.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            int balance=500;//yo value chai database bata aaunu paryoo yo ta aafai haleko ho maile harek time ghatauda 500 matra liraxa paisa
                            //ghatara updated value balance ko banaunu paryo ani feri paisa ghatauda update vako value bata hunu paryoo

                            int bal = Integer.parseInt(name);

                            // int balance = dataSnapshot.child("balance").getValue().toString();;




                          //  String user = dataSnapshot.child("Balance").getValue().toString();//yo query le kaam garira xaina
                            //int balance= Integer.valueOf(user);


                            int amount = balance - bal;
                            dataSnapshot.getRef().getParent().child("Balance").setValue(amount);
                           // dataSnapshot.getRef().getParent().child("Balance").setValue("274");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });*/

                    //setting edittext to blank again
                    enter.setText("");

                    //displaying a success toast
                    AlertDialog.Builder alertDialog=new AlertDialog.Builder(SecondActivity.this);
                    alertDialog.setTitle("CONFIRM");
                    alertDialog.setCancelable(false);
                    alertDialog.setMessage("Are you sure you want to make Payment?");
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
<<<<<<< HEAD

                            usersRef2= FirebaseDatabase.getInstance().getReference("users").child("D62lyNbU1UWk3sAw2mm5LxOgTZN2").child("Balance");
                            //Toast.makeText(SecondActivity.this, "Your amount has been ", Toast.LENGTH_LONG).show();
                            //Query update=usersRef.child("users").orderByKey().equalTo("users");
                            usersRef2.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    long bal = Long.parseLong(name);
                                    long balance = Long.parseLong(dataSnapshot.getValue().toString() );
                                    long amount = balance + bal;
                                    usersRef2.setValue(amount);

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
=======
                                

                           /* usersRef = FirebaseDatabase.getInstance().getReference("users").child("D62lyNbU1UWk3sAw2mm5LxOgTZN2");
>>>>>>> 1252f4316be6907eb8f57c3ed5e764cd97e4af79

                            });
                            //Toast.makeText(SecondActivity.this, "Your amount has been ", Toast.LENGTH_LONG).show();



                            mAuth=FirebaseAuth.getInstance();
                            currentuserid=mAuth.getCurrentUser().getUid();
                            usersRef= FirebaseDatabase.getInstance().getReference("users").child(currentuserid).child("Balance");
                           // Query updateQuery=usersRef.child("users").orderByKey().equalTo("users");
                            usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                                    long bal = Long.parseLong(name);
                                    long balance = Long.parseLong(dataSnapshot.getValue().toString() );// long balance = dataSnapshot.getValue(Long.class) ;
                                    long amount = balance - bal;
                                   // dataSnapshot.getRef().getParent().child("Balance").setValue(amount);
                                    usersRef.setValue(amount);
                                    usersRef2= FirebaseDatabase.getInstance().getReference("users").child("D62lyNbU1UWk3sAw2mm5LxOgTZN2").child("Balance");
                            //Query update=usersRef.child("users").orderByKey().equalTo("users");
                            usersRef2.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    long bal = Long.parseLong(name);
                                    long balance = Long.parseLong(dataSnapshot.getValue().toString() );
                                    long amount = balance + bal;
                                    usersRef2.setValue(amount);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                                    // dataSnapshot.getRef().getParent().child("Balance").setValue("274");
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });

                            Intent d=new Intent(SecondActivity.this,MainActivity.class);
                            startActivity(d);
                            Toast.makeText(SecondActivity.this, "Your amount has been deducted", Toast.LENGTH_LONG).show();

                        }
                    });

                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent d=new Intent(SecondActivity.this,MainActivity.class);
                            startActivity(d);

                        }
                    });
                    alertDialog.show();

                } else {
                    //if the value is not given displaying a toast
                    Toast.makeText(SecondActivity.this, "Please enter a number", Toast.LENGTH_LONG).show();
                }
            }

        });

        mAuth=FirebaseAuth.getInstance();
        currentuserid=mAuth.getCurrentUser().getUid();
        usersRef= FirebaseDatabase.getInstance().getReference().child("users").child(currentuserid);
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String user=dataSnapshot.child("username").getValue().toString();


                    a.setText("Sender : " + user);



                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        usersRef= FirebaseDatabase.getInstance().getReference().child("transaction").child("-Lnm_WDswWcM3VK3Ua17");
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String Amount=dataSnapshot.child("amount").getValue().toString();
                    String Vat=dataSnapshot.child("vat").getValue().toString();
                    String Total=dataSnapshot.child("total").getValue().toString();

                    e.setText("Amount :Rs. " + Amount);
                    f.setText("Vatamount :Rs. " + Vat);
                    g.setText("TotalAmount :Rs. " + Total);



                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });


        usersRef= FirebaseDatabase.getInstance().getReference().child("company").child("Qvo5NQsir4OpaHRdzgaqcWLRfyC2");
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String Name=dataSnapshot.child("name").getValue().toString();
                    String Txn=dataSnapshot.child("txn").getValue().toString();

                    c.setText("Receiver : " + Name);
                    b.setText("Txnnumber : " + Txn);



                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });
    }
}
