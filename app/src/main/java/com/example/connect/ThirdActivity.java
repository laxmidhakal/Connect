package com.example.connect;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class ThirdActivity extends AppCompatActivity {
    private TextView username,address,date,balance;
    private Button ok;
    private FirebaseAuth mAuth;
    private DatabaseReference usersRef;
    private String currentuserid;

   /* private Date getDate(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time) * 1000));
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        mAuth= FirebaseAuth.getInstance();
        String dateStr = "04/05/2010";

        SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
        Date dateObj = curFormater.parse(dateStr);
        SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

        String newDateStr = postFormater.format(dateObj);


        //usersRef= FirebaseDatabase.getInstance().getReference().child("transaction");
        //date=(TextView) findViewById(R.id.date);

        username=(TextView) findViewById(R.id.name);
        balance=(TextView) findViewById(R.id.balance);

        address=(TextView) findViewById(R.id.company);
       // amount=(EditText) findViewById(R.id.amount);
        //vat=(EditText) findViewById(R.id.vat);
        ok=(Button) findViewById(R.id.ok);


        mAuth=FirebaseAuth.getInstance();

        currentuserid=mAuth.getCurrentUser().getUid();
        usersRef= FirebaseDatabase.getInstance().getReference().child("users").child(currentuserid);
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                    String user=dataSnapshot.child("username").getValue().toString();
                   /* Long timestamp=(Long) dataSnapshot.child("time").getValue();
                    System.out.println(timestamp);*/
                    String bal=dataSnapshot.child("Balance").getValue().toString();
                   // String Date=dataSnapshot.child("time").getValue().toString();


                    username.setText("Name : " + user);
                    balance.setText("Balance : " + bal);
                   // date.setText("Balance : " +Date);


                    //date.setText("Date"+timestamp);

                   // Long timestamp = (Long) snapshot.getValue();

                   /* private String getDate(long time) {
                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                        cal.setTimeInMillis(Date * 1000);
                       // String Date=dataSnapshot.child("time").getValue().toString();
                        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
                        return date;
                    }*/
                  /*  private Date getDate(long time) {
                        Calendar cal = Calendar.getInstance();
                        TimeZone tz = cal.getTimeZone();//get your local time zone.
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                        sdf.setTimeZone(tz);//set time zone.
                        String localTime = sdf.format(new Date(time) * 1000));
                        Date date = new Date();
                        try {
                            date = sdf.parse(localTime);//get local date
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return date;
                    }*/




                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //usersRef.setValue(ServerValue.TIMESTAMP);
        usersRef= FirebaseDatabase.getInstance().getReference().child("company").child("Qvo5NQsir4OpaHRdzgaqcWLRfyC2");
        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){


                    String Txn=dataSnapshot.child("txn").getValue().toString();

                    address.setText("Txn : " + Txn);



                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SaveRegisterInformation();
            }

           /* private void SaveRegisterInformation() {
                HashMap usermap= new HashMap();
                usermap.put("time", ServerValue.TIMESTAMP);
                usermap.put("balance","0");
                usermap.put("amount","200");
                usermap.put("vat","26");
                usermap.put("total","226");
                usersRef.push().setValue(usermap);
                usersRef.updateChildren(usermap).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()){
                            //SendUserToMainActivity();
                            Toast.makeText(ThirdActivity.this,"Your transaction is saved successfully",Toast.LENGTH_SHORT).show();

                        }
                        else {
                            String message=task.getException().getMessage();
                            Toast.makeText(ThirdActivity.this,"Error :"+message,Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }*/
        });

    }
}
